// Registering SW
if ("serviceWorker" in navigator) {
  window.addEventListener("load", () => {
    navigator.serviceWorker
      .register("/service-worker.js")
      .then((registration) => {
        console.log("Service Worker registered successfully");
      })
      .catch((error) => {
        console.error("Service Worker registration failed", error);
      });
  });
}

// Show a custom prompt or UI to the user
window.addEventListener("beforeinstallprompt", (e) => {
  e.preventDefault();
});

const gridContainer = document.querySelector(".grid-container");
const fieldSelect = document.querySelector("#field-select");
const chipContainer = document.querySelector("#chip-container");
const showCompletedBtn = document.querySelector("#show-completed");
const showPendingBtn = document.querySelector("#show-pending");

let todos = [];
let filteredTodos = [];
let selectedFields = ["id", "userId", "title", "completed"];

// Fetch todos from the API
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    todos = data;
    renderTodos(todos);
    renderChips();
  })
  .catch((error) => console.error(error));

// Render todos
function renderTodos(todosToRender) {
  gridContainer.innerHTML = "";
  todosToRender.forEach((todo) => {
    const card = document.createElement("div");
    card.classList.add("card");

    const header = document.createElement("div");
    header.classList.add("card-header");

    const title = document.createElement("h3");
    title.classList.add("card-title");
    title.textContent = `Todo ${todo.id}`;
    header.appendChild(title);

    const status = document.createElement("span");
    status.classList.add("card-status");
    status.textContent = todo.completed ? "Completed" : "Pending";
    header.appendChild(status);

    card.appendChild(header);

    const body = document.createElement("div");
    body.classList.add("card-body");

    selectedFields.forEach((field) => {
      if (field !== "id" && field !== "completed") {
        const fieldElement = document.createElement("p");
        fieldElement.textContent = `${field}: ${todo[field]}`;
        body.appendChild(fieldElement);
      }
    });

    card.appendChild(body);

    gridContainer.appendChild(card);
  });
}

// Filter todos based on selected fields
fieldSelect.addEventListener("change", () => {
  const selectedOptions = Array.from(
    fieldSelect.selectedOptions,
    (option) => option.value,
  );
  selectedFields = [...new Set([...selectedFields, ...selectedOptions])];
  renderTodos(filteredTodos.length > 0 ? filteredTodos : todos);
  renderChips();
});

// Show completed todos
showCompletedBtn.addEventListener("click", () => {
  filteredTodos = todos.filter((todo) => todo.completed);
  renderTodos(filteredTodos);
});

// Show pending todos
showPendingBtn.addEventListener("click", () => {
  filteredTodos = todos.filter((todo) => !todo.completed);
  renderTodos(filteredTodos);
});

// Render chips
function renderChips() {
  chipContainer.innerHTML = "";
  selectedFields.forEach((field) => {
    const chip = document.createElement("div");
    chip.classList.add("chip");
    chip.textContent = `${field} `;
    const closeIcon = document.createElement("span");
    closeIcon.textContent = "×";
    chip.appendChild(closeIcon);
    chip.addEventListener("click", () => {
      removeChip(field);
    });
    chipContainer.appendChild(chip);
  });
}

// Remove chip
function removeChip(field) {
  selectedFields = selectedFields.filter((f) => f !== field);
  fieldSelect.value = selectedFields;
  renderTodos(filteredTodos.length > 0 ? filteredTodos : todos);
  renderChips();
}

