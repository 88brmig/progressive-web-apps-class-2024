const API_URL = "https://jsonplaceholder.typicode.com/todos"

const todosEl = document.getElementById("todos")
const idElement = document.getElementById("id-check")
const userElement = document.getElementById("user-check")
const titleElement = document.getElementById("title-check")
const completeElement = document.getElementById("complete-check")
const onlyCompletedElement = document.getElementById("only-completed")
const pendingCompletedElement = document.getElementById("only-pending")

idElement.onchange = updateState
userElement.onchange = updateState
titleElement.onchange = updateState
completeElement.onchange = updateState
onlyCompletedElement.onchange = updateState
pendingCompletedElement.onchange = updateState

const state = {
    id: true,
    userId: true,
    title: true,
    completed: true,
    showCompleted: true,
    showPending: true,
    todos: []
}

function refreshTodos() {
    getTodos().then(todos => state.todos = todos).finally(() => {
        todosEl.innerHTML = ""
        state.todos.forEach(todo => {
            todosEl.innerHTML += TodoItem(todo)
        })
    })
}

function onStateChange() {
    document.querySelectorAll("li").forEach(li => {
        const isCompleted = li.querySelector("span[name=completed]").value

        li.querySelectorAll("span[name=id], span[name=userId], span[name=title], span[name=completed]").forEach(span => {
            !state[span.id] ? span.classList.add("hide") : span.classList.remove("hide")
        })

        // No condition is being set over tasks
        if (state.showCompleted && state.showPending) {
            console.log("1")
            return li.classList.remove("hide")
        }

        // Show only completed
        if (state.showCompleted && isCompleted) {
            console.log("2")
            return li.classList.remove("hide")
        }

        // Show only pending
        if (state.showPending && !isCompleted) {
            console.log("3")
            return li.classList.remove("hide")
        }

        // Hide if neither conditions are met
        console.log("4")
        return li.classList.add("hide")
    })
}

function updateState() {
    state.id = idElement.checked
    state.title = titleElement.checked
    state.userId = userElement.checked
    state.completed = completeElement.checked
    state.showCompleted = onlyCompletedElement.checked
    state.showPending = pendingCompletedElement.checked

    onStateChange()
}

function TodoItem(item) {
    return `
    <li class="todo-item">
        <span id="id" name="id" class="todo-id">${item.id}</span>
        <span id="userId" name="userId" class="todo-user">${item.userId}</span>
        <span id="title" name="title" class="todo-title">${item.title}</span>
        <span id="completed" name="completed" class="todo-completed">${item.completed}</span>
    </li>
    `
}

async function getTodos() {
    try {
        const res = await fetch(API_URL)
        if (!res.ok) throw new Error("Received status code: " + res.status)

        return await res.json()
    } catch (error) {
        console.log(error)
    }
}

refreshTodos()